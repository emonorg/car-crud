import MongoHandler from '../ayberk/database/Mongo.database';
import App from '../ayberk/App';
import validateEnv from '../ayberk/EnvValidator';

// Controllers
import CarController from './modules/car/Car.controller';

// Services
import AppService from './modules/car/Car.service';

// Validate the env variables: Ensure all the params are passed
validateEnv();

// Prod setup
const app = new App(
  [
    MongoHandler.connect('default', process.env.MONGO_URI),
  ],
  [
    new CarController(new AppService()),
  ]
);

app.listen();
