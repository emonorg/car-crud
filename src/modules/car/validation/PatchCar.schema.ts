/**
 * Define a JSON schema.
 */
 export const patchCarSchema = {
  additionalProperties: false,
  type: 'object',
  required: ['_id'],
  properties: {
    _id: {
      type: 'string',
      pattern: '^[a-f\\d]{24}$',
    },
    name: {
      type: 'string',
    },
    brand: {
      type: 'string',
    },
    color: {
      type: 'string',
    },
    suppliedCountry: {
      type: 'string',
    },
    maxSpeed: {
      type: 'number',
    },
    description: {
      type: 'string',
    },
    image: {
      type: 'string',
    },
    status: {
      type: 'boolean',
    },
  },
};
