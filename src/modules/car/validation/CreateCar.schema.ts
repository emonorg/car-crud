/**
 * Define a JSON schema.
 */
 export const createCarSchema = {
  additionalProperties: false,
  type: 'object',
  required: ['name', 'brand', 'color', 'suppliedCountry', 'maxSpeed', 'description', 'image'],
  properties: {
    name: {
      type: 'string',
    },
    brand: {
      type: 'string',
    },
    color: {
      type: 'string',
    },
    suppliedCountry: {
      type: 'string',
    },
    maxSpeed: {
      type: 'number',
    },
    description: {
      type: 'string',
    },
    image: {
      type: 'string',
    },
  },
};
