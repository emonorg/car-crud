/**
 * Define a JSON schema.
 */
 export const deleteCarSchema = {
  additionalProperties: false,
  type: 'object',
  required: ['_id'],
  properties: {
    _id: {
      type: 'string',
      pattern: '^[a-f\\d]{24}$',
    },
  },
};
