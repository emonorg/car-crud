import { Router, Request, Response, NextFunction } from 'express';
import Controller from '../../../ayberk/interfaces/Controller.interface';
import sendResponse from '../../../ayberk/ResponseBuilder';
import ResponseType from '../../../ayberk/types/Response.type';
import CarService from './Car.service';
import Validator from '../../../ayberk/validators/ajv';
import {createCarSchema} from './validation/CreateCar.schema';
import { patchCarSchema } from './validation/PatchCar.schema';
import { deleteCarSchema } from './validation/DeleteCar.schema';
export default class CarController implements Controller {
  public router: Router = Router();
  /**
   * Define the base path (base route) of the controller by defining 'path' variable
   */
  public path = '/car';

  constructor(private carService: CarService) {
    this.initializeRoutes();
  }

  /**
   * Define the sub-routes (with methods) inside this function
   */
  private initializeRoutes(): void {
    this.router.get(`${this.path}`, this.getCars);
    this.router.get(`${this.path}/:carId`, this.getCar);
    this.router.post(`${this.path}`, Validator.validate({ body: createCarSchema }), this.createCar);
    this.router.patch(`${this.path}`, Validator.validate({ body: patchCarSchema }), this.patchCar);
    this.router.delete(`${this.path}`, Validator.validate({ body: deleteCarSchema }), this.deleteCar);
  }

  /**
   * Create car
   * @param request 
   * @param response 
   * @param next 
   */
  private createCar = async (request: Request, response: Response, next: NextFunction) => {
    try {
      sendResponse(response, 200, new ResponseType({
        success: true,
        message: 'Car has been created successfully!'
      }, await this.carService.createCar(request.body)));
    } catch(err) {
      next(err);
    }
  }

  /**
   * Get a list of all cars (Only meta-data is fetched)
   * @param request 
   * @param response 
   * @param next 
   */
  private getCars = async (request: Request, response: Response, next: NextFunction) => {
    try {
      sendResponse(response, 200, new ResponseType({
        success: true,
        message: 'Cars list has been fetched!'
      }, await this.carService.getCars()));
    } catch(err) {
      next(err);
    }
  }

  /**
   * Get data of a car based on carId
   * @param request 
   * @param response 
   * @param next 
   */
  private getCar = async (request: Request, response: Response, next: NextFunction) => {
    try {
      sendResponse(response, 200, new ResponseType({
        success: true,
        message: 'Cars list has been fetched!'
      }, await this.carService.getCar(request.params.carId)));
    } catch(err) {
      next(err);
    }
  }

  /**
   * Patch a car based on carId
   * @param request 
   * @param response 
   * @param next 
   */
  private patchCar = async (request: Request, response: Response, next: NextFunction) => {
    try {
      sendResponse(response, 200, new ResponseType({
        success: true,
        message: 'Car updated successfully!'
      }, await this.carService.patchCar(request.body)));
    } catch(err) {
      next(err);
    }
  }

  /**
   * Delete a car based on carId
   * @param request 
   * @param response 
   * @param next 
   */
  private deleteCar = async (request: Request, response: Response, next: NextFunction) => {
    try {
      sendResponse(response, 200, new ResponseType({
        success: true,
        message: 'Car deleted successfully!'
      }, await this.carService.deleteCar(request.body)));
    } catch(err) {
      next(err);
    }
  }
}
