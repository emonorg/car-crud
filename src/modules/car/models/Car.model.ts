import Mongoose from 'mongoose';

export interface ICar {
  _id: string;
  name: string;
  brand: string;
  color: string;
  suppliedCountry: string;
  image: string;
  description: string;
  maxSpeed: number;
  createdAt: Date;
  updatedAt: Date;
  status: boolean;
}

export default class Car {
  private static carSchema = new Mongoose.Schema<ICar>(
    {
      name: {
        type: String,
        required: true,
      },
      brand:  {
        type: String,
        required: true,
      },
      color:  {
        type: String,
        required: true,
      },
      suppliedCountry:  {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      image: {
        type: String,
        required: true,
      },
      maxSpeed: {
        type: Number,
        required: true,
      },
      createdAt: {
        type: Date,
        required: true,
      },
      updatedAt: {
        type: Date,
        required: true,
      },
      status: {
        type: Boolean,
        required: true,
      },
    }
  );
  // tslint:disable-next-line: member-ordering
  public static model = Mongoose.model<ICar>('Car', this.carSchema, 'Cars');
}
