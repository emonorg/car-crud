import HttpException from '../../../ayberk/exceptions/HttpException';
import Car, { ICar } from './models/Car.model';

export default class AppService {

  public async createCar(newCar: ICar): Promise<ICar> {
    // Make sure that name is unique
    const car = await Car.model.findOne({name: newCar.name, status: true});
    // Send error if the provided name is not unique
    if (car) { throw new HttpException(400, 'A car with this name exists!'); }
    return await Car.model.create({...newCar, createdAt: new Date(), updatedAt: new Date(), productionDate: new Date(), status: true});
  }

  public async getCars(): Promise<ICar[]> {
    return await Car.model.find({status: true}, 'name description image').sort('-createdAt');
  }

  public async getCar(carId: string): Promise<ICar[]> {
    return await Car.model.findOne({_id: carId, status: true});
  }

  public async patchCar(car: ICar): Promise<ICar> {
    const updateResult = await Car.model.updateOne({_id: car._id}, {...car});
    // Check if any record is updated
    if (updateResult.modifiedCount === 0) {
      // Send error if no records are effected
      throw new HttpException(400, 'Invalid carId or no updated field!');
    }
    return await Car.model.findOne({_id: car._id});
  }

  public async deleteCar(carId: string): Promise<ICar> {
    const updateResult = await Car.model.findOneAndUpdate({_id: carId, status: true}, {status: false});
    // Check if any record is effected
    if (!updateResult) {
      throw new HttpException(400, 'Invalid carId!');
    } return updateResult;
  }
}
