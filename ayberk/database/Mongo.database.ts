import Mongoose from 'mongoose';
import Logger from '../Logger';

export default class MongoHandler {
  // Connect to mongo instance and add it to the registry array
  public static async connect(name: string, uri: string, options?: object): Promise<void> {
    Logger.info('Connecting to MongoDB instance...');
    await Mongoose.connect(uri, options);
    Logger.success('Mongo connected successfully!');
  }
}
