import 'dotenv/config';

import Express from 'express';
import CookieParser from 'cookie-parser';
import BodyParser from 'body-parser';
import Morgan from 'morgan';

import Controller from './interfaces/Controller.interface';
import ErrorHandler from './middlewares/ErrorHandler.middleware';
import Logger from './Logger';

export default class App {
  public app: Express.Application;
  constructor(initServices: any[], controllers: Controller[]) {
    this.app = Express();

    this.initializeMiddlewares();
    this.initControllers(controllers);

    // Error handler middleware
    this.app.use(ErrorHandler);
  }

  public listen(): void {
    this.app.listen(process.env.APP_PORT);
    Logger.showInfoBox();
  }

  private initializeMiddlewares(): void {
    this.app.use(Morgan('dev'));
    this.app.use(BodyParser.json());
    this.app.use(CookieParser());
  }

  private initControllers(controllers: Controller[]): void {
    // Load controllers
    for (const controller of controllers) {
      this.app.use('/api', controller.router);
    }

    // Health check endpoint
    this.app.get('/health-check', (req: Express.Request, res: Express.Response) => {
      return res.json({isUp: true, time: new Date()});
    });
  }
}
