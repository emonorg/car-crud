# How to run
Simply use the following command :)
```
sudo docker-compose up -d
```

# About project
I have developed a boilerplate for bootstrapping my express projects called Ayberk. Ayberk handles
creating and serving different layers of project. You can check it in `ayberk` directory.

# Endpoints:
I have included Postman collection.